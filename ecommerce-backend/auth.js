// commented generated code by vscode
// const { response } = require("express");
// const { request, response } = require("express");
const jwt = require("jsonwebtoken");
const secretCode = "e-commerceAPI";


// Creating token for users attempting to login
module.exports.createAccessToken = (userDetails) => {
    const data = {
        id: userDetails.id,
        email: userDetails.email,
        isAdmin: userDetails.isAdmin
    }

    return jwt.sign(data, secretCode, {});
}

// Check token
module.exports.verify = (request, response, next) => {
    let token = request.headers.authorization;

    if (typeof token === "undefined") {
        return response.send({auth: "Failed, token required."})
    }
    else {
        token = token.slice(7); // Bearer
        
        // Verify token legitimacy
        jwt.verify(token, secretCode, function(err, decodedToken) {
            if (err) {
                return response.send({
                    auth: "Failed!",
                    message: err.message
                })
            }
            else {
                request.user = decodedToken;
                next();
            }
        })
    }
}

module.exports.verifyAdmin = (request, response, next) => {
    // check if user is admin to continue
    if (request.user.isAdmin) {
        next();
    }
    else {
        return response.send({
            auth: "Failed",
            message: "Action Forbidden"
        })
    }
}