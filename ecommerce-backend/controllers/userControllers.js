const User = require("../models/User");
const Product = require("../models/Product");
const bcrypt = require("bcrypt");
const auth = require("../auth");
const { response } = require("express");

// CREATE USER
module.exports.registerUser = (request, response) => {
    const hashedPassword = bcrypt.hashSync(request.body.password, 10);
    
    let createUser = new User({
        firstName: request.body.firstName,
        lastName: request.body.lastName,
        email: request.body.email,
        password: hashedPassword,
        mobileNo: request.body.mobileNo
    });

    createUser.save()
    .then(result => response.send(result))
    .catch(error => response.send(error))
}

// USER AUTHENTICATION OR LOGIN
module.exports.loginUser = (request, response) => {
    User.findOne({email: request.body.email})
    .then(foundUser => {
        if (foundUser === null) {
            return response.send({message: "User doesn't exist!"})
        }
        else {
            const passwordMatched = bcrypt.compareSync(request.body.password, foundUser.password);

            if (passwordMatched) {
                return response.send({accessToken: auth.createAccessToken(foundUser)});
            }
            else {
                return response.send({message: "Incorrect Password."})
            }
        }
    })
}

// RETRIEVE USER DETAILS
module.exports.getUserDetails = (request, response) => {
    User.findById(request.user.id)
    .then(result => response.send(result))
    .catch(error => response.send(error))   
}

// CHECK IF EMAIL EXISTS
module.exports.checkEmailExists = (request, response) => {
	User.findOne({email: request.body.email})
	.then(result => {
		if(result === null){
			return response.send(false);
		} else {
			return response.send(true)
		}
	})
	.catch(error => response.send(error));
}

module.exports.updateAdmin = (request, response) => {
    let archive = {
        isAdmin: true
    }

    User.findByIdAndUpdate(request.params.userId, archive, {new: true})
    .then(updateAdminResult => response.send(updateAdminResult))
    .catch(error => response.send(error))
}

// CHECKOUT ORDERS FOR LOGGED USER (SINGLE ORDER) 
module.exports.createSingleOrder = async (request, response) => {
    if (request.user.isAdmin) {
        return response.send({message: "Action Forbidden"})
    }

    let isUserUpdated = await User.findById(request.user.id).then(user => {
        
        let newOrder = {
            totalAmount: request.body.totalAmount,
            products: [{
                    productId: request.body.productId,
                    quantity: request.body.quantity
                }]     
        }

        user.orders.push(newOrder);
        console.log(newOrder);

        return user.save()
        .then(user => true)
        .catch(err => err.message);
    });

    if (isUserUpdated !== true) {
        return response.send({message: isUserUpdated});
    }

    let isOrderCreated = await Product.findById(request.body.productId).then(product => {
        let checkoutOrder = {
            orderId: product.orders.id,
            userId: request.user.id,
            quantity: request.body.quantity
        }

        product.orders.push(checkoutOrder);

        return product.save()
        .then(course => true)
        .catch(err => err.message);
    });

    if (isOrderCreated !== true) {
        return response.send({message: isOrderCreated});
    }

    if (isUserUpdated && isOrderCreated) {
        // return response.send({message: "Thank you for your order!"});
        return response.send(true);
    }
}

// CHECKOUT ORDER FOR LOGGED USER (MULTIPLE ORDERS) 
module.exports.createMultipleOrders = async (request, response) => {
    if (request.user.isAdmin) {
        return response.send({message: "Action Forbidden"})
    }
     User.findById(request.user.id).then(user => {
        let newOrder = {
            totalAmount: request.body.totalAmount,
            products: request.body.products
        }

        user.orders.push(newOrder);
        let indexOrderId = user.orders[user.orders.length - 1];
        newOrder.products.forEach(function(product) {
            Product.findById(product.productId).then(products => {
                let checkoutOrder = {
                    orderId: indexOrderId.id,
                    userId: request.user.id,
                    quantity: product.quantity
                }
                products.orders.push(checkoutOrder);
                return products.save().then(course => true).catch(err => err.message)
            })
        })
        return user.save().then(user => true).catch(err => err.message)
     }) 
    //  return response.send({message: "Thank you for your order!"});
    return response.send(true);
}

// VIEW ORDERS FOR LOGGED USERS
module.exports.getUserOrders = (request, response) => {
    User.findById(request.user.id)
    .then(result => response.send(result))
    .catch(error => response.send(error))
}

// VIEW ALL ACTIVE ORDERS (ADMIN ONLY)
module.exports.getAllOrders = (request, response) => {
    User.find({isAdmin: false, orders: {$ne:[]}})
    .then(result => response.send(result))
    .catch(error => response.send(error))
}