const User = require("../models/User");
const Product = require("../models/Product");
const auth = require("../auth");
const { response } = require("express");

// CREATE PRODUCT (ADMIN ONLY)
module.exports.createProduct = (request, response) => {
    let createProduct = new Product({
        name: request.body.name,
        description: request.body.description,
        price: request.body.price
    });
    
    createProduct.save()
    .then(result => response.send(result))
    .catch(error => response.send(error))
}

// RETRIEVE ALL ACTIVE PRODUCTS
module.exports.getActiveProducts = (request, response) => {
    Product.find({isActive: true})
    .then(result => response.send(result))
    .catch(error => response.send(error))
}

// RETRIEVE SINGLE PRODUCT
module.exports.getSingleProduct = (request, response) => {
    Product.findById(request.params.productId)
    .then(result => response.send(result))
    .catch(error => response.send(error))
}

// RETRIEVE ALL PRODUCTS
module.exports.getAllProducts = (request ,response) => {
	Product.find({})
	.then(result => response.send(result))
	.catch(error => response.send(error));
}

// UPDATE PRODUCT (ADMIN ONLY)
module.exports.updateProduct = (request, response) => {
    // console.log(request.params.productId);
    let update = {
        name: request.body.name,
        description: request.body.description,
        price: request.body.price
    }

    Product.findByIdAndUpdate(request.params.productId, update, {new: true})
    .then(updateResult => response.send(updateResult))
    .catch(error => res.send(error))
}

// DELETE PRODUCT (ADMIN ONLY)
module.exports.deleteProduct = (request, response) => {
    Product.findByIdAndDelete(request.params.productId)
    .then(activateResult => response.send(activateResult))
    .catch(error => res.send(error))
}

// ARCHIVE PRODUCT (ADMIN ONLY)
module.exports.archiveProduct = (request, response) => {
    // console.log(request.params.productId);
    let archive = {
        isActive: false
    }

    Product.findByIdAndUpdate(request.params.productId, archive, {new: true})
    .then(archiveResult => response.send(archiveResult))
    .catch(error => result.send(error))
}

// UNARCHIVE PRODUCT (ADMIN ONLY)
module.exports.unarchiveProduct = (request, response) => {
    // console.log(request.params.productId);
    let unarchive = {
        isActive: true
    }

    Product.findByIdAndUpdate(request.params.productId, unarchive, {new: true})
    .then(activateResult => response.send(activateResult))
    .catch(error => result.send(error))
}