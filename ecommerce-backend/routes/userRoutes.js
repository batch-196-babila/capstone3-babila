const express = require("express");
const router = express.Router();

const userControllers = require("../controllers/userControllers");

const auth = require("../auth");
const { verify, verifyAdmin } = auth;

// REGISTER USER
router.post('/', userControllers.registerUser);

// lOGIN
router.post('/login', userControllers.loginUser);

// RETRIEVE USER DETAILS
router.get('/getUserDetails', verify, userControllers.getUserDetails);

// CHECK EMAIL EXISTS
router.post('/checkEmailExists', userControllers.checkEmailExists);

// UPDATE ROLE TO ADMIN (ADMIN ONLY)
router.put('/updateAdmin/:userId', verify, verifyAdmin, userControllers.updateAdmin);

// CREATE SINGLE ORDER FOR LOGGED USER
router.post('/checkoutSingleOrder', verify, userControllers.createSingleOrder);

// CREATE MULTIPLE ORDERS FOR LOGGED USER
router.post('/checkoutMultipleOrders', verify, userControllers.createMultipleOrders);

// GET LOGGED USER ORDER 
router.get('/viewOrders', verify, userControllers.getUserOrders);

// GET ALL ORDERS FROM ALL USERS (ADMIN ONLY)
router.get('/viewAllOrders', verify, verifyAdmin, userControllers.getAllOrders);

module.exports = router;
