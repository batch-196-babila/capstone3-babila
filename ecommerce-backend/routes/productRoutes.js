const express = require("express");
const router = express.Router();
const productControllers = require("../controllers/productControllers");
const auth = require("../auth");
const { verify, verifyAdmin } = auth;

// RETRIEVE ACTIVE PRODUCTS
router.get('/active', productControllers.getActiveProducts);

// RETRIEVE SINGLE PRODUCT
router.get('/getSingleProduct/:productId', productControllers.getSingleProduct);

// CREATE PRODUCT (ADMIN ONLY)
router.post('/create', verify, verifyAdmin, productControllers.createProduct);

// RETRIEVE ALL PRODUCTS (AVAILABLE OR NOT)
router.get('/', verify, verifyAdmin, productControllers.getAllProducts);

// UPDATE PRODUCT (ADMIN ONLY)
router.put('/update/:productId', verify, verifyAdmin, productControllers.updateProduct);

// DELETE PRODUCT (ADMIN ONLY)
router.delete('/delete/:productId', verify, verifyAdmin, productControllers.deleteProduct);

// ARCHIVE PRODUCT (ADMIN ONLY)
router.put('/archive/:productId', verify, verifyAdmin, productControllers.archiveProduct);

// UNARCHIVE PRODUCT (ADMIN ONLY)
router.put('/unarchive/:productId', verify, verifyAdmin, productControllers.unarchiveProduct);

module.exports = router;