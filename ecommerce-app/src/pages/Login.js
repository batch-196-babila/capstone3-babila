import { useEffect, useState, useContext } from 'react';
import { Form, Button } from 'react-bootstrap';
import { Navigate, Link } from 'react-router-dom';
import UserContext from '../UserContext';
import '../style/styles.css';
import Swal from 'sweetalert2';

export default function Login() {

    const {user, setUser} = useContext(UserContext);

    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [isActive, setIsActive] = useState(false);

    function userLogin(e) {
        e.preventDefault();

        // LOGIN AUTHENTICATION
        fetch('http://localhost:4000/users/login', {
            method: 'POST',
            headers: {
                'Content-Type': "application/json"
            },
            body: JSON.stringify({
                email: email,
                password: password
            })
        })
        .then(response => response.json())
        .then(data => {
            if (typeof data.accessToken !== "undefined") {
                localStorage.setItem('token', data.accessToken);
                retrieveUserDetails(data.accessToken);
            }
            else {
                Swal.fire({
                    title: "Authentication Failed",
                    icon: "error",
                    text: "Check your details!"
                })
            }
        })

        setEmail('');
        setPassword('');
    }

    const retrieveUserDetails = (token) => {
        fetch('http://localhost:4000/users/getUserDetails', {
            headers: {
                Authorization: `Bearer ${token}`
            }
        })
        .then(response => response.json())
        .then(data => {
            // console.log(data);

            setUser({
                id: data._id,
                isAdmin: data.isAdmin
            })
        })
    }

    useEffect(() => {
        if (email !== '' && password !== '')  {
            setIsActive(true);
        }
        else {
            setIsActive(false);
        }
    }, [email, password]);

    return (
        // NAVIGATE TO HOME
        (user.id !== null) ?
            <Navigate to="/" />
        :
        <>
        <Form className="login-form" onSubmit={e => userLogin(e)}>
            <h4 className="title-header">L O G I N</h4>
            <p className="subtitle-header">Please enter your email and password:</p>
            <Form.Group className="login-input" controlId="userEmail" >
                <Form.Control 
                    className="input-height"
                    type="email" 
                    placeholder="Email" 
                    required 
                    value={email}
                    onChange={e => setEmail(e.target.value)}
                />
            </Form.Group>

            <Form.Group className="login-input" controlId="password">
                <Form.Control 
                    className="input-height"
                    type="password" 
                    placeholder="Password" 
                    required
                    value={password}
                    onChange={e => setPassword(e.target.value)}
                />
            </Form.Group>

            { isActive ?
                <Button className="login-button" variant="outline-primary" type="submit" id="submitBtn">LOGIN</Button>
                :
                <Button className="login-button" variant="outline-primary" type="submit" id="submitBtn" disabled>LOGIN</Button>
            }
            
            <p className="subtitle-header">Don't have an account? <Link to="/register">Register Here</Link></p>
        </Form>
        </>
    )
}