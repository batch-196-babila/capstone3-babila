import Banner from '../components/Banner';
import Highlights from '../components/Highlights';

// HOME CONTAINS BANNER & HIGHLIGHTS 
export default function Home() {
    return (
        <>
            <Banner />
            <Highlights />
        </>
    )
}