import { useEffect, useState, useContext } from "react";
import { Button, Table } from "react-bootstrap";
import { Link, useNavigate } from "react-router-dom";
import OrderList from '../components/OrderList';
import UserContext from '../UserContext';

export default function AdminViewOrders() {
    const { user } = useContext(UserContext);
    const redirect = useNavigate();
    const [orders, setOrders] = useState([]); 
    
    useEffect(() => {
        fetch("http://localhost:4000/users/viewAllOrders", {
             headers: {
                Authorization: `Bearer ${localStorage.getItem('token')}`
            }
        })
        .then(response => response.json())
        .then(data => {
            // console.log(data);
            setOrders(data.map(order => {
                return (
                    <OrderList key={order._id} orderProp={order} />
                )
            }))
        })
    }, []);

    return (
        (user.isAdmin === false || user.id === null) ?
            redirect('/')
        :
        <>
            <Table bordered className="mt-5">
                <thead>
                    <tr>
                        <th>Customer Name</th>
                        <th>Product Id</th>
                        <th>Total Amount</th>
                        <th>Quantity</th>
                    </tr>
                </thead>
                <tbody>
                    { orders }
                </tbody>
            </Table>
            <Button as={Link} to="/admin/dashboard" className="mt-5" variant="outline-dark">
                BACK TO DASHBOARD
            </Button> 
        </>
    );
}