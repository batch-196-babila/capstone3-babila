import { useState, useEffect, useContext } from 'react';
import { Table, Button } from 'react-bootstrap';
import { Link, useNavigate } from 'react-router-dom';
import CreateProduct from '../components/CreateProduct';
import ProductList from '../components/ProductList';
import UserContext from '../UserContext';

export default function Dashboard() {

    const { user } = useContext(UserContext);
    const redirect = useNavigate();

    const [products, setProducts] = useState([]);
   
    useEffect(() => {
        fetch('http://localhost:4000/products', {
            headers: {
                'Content-Type': 'application/json',
                Authorization: `Bearer ${localStorage.getItem('token')}`
            }
        })
        .then(res => res.json())
        .then(data => {
            // console.log(data);
            const productList = (data.map(product => {
                return (
                    <ProductList key={product._id} productProp={product} />
                )
            }))
            setProducts(productList);
        });
    }, []);

    return (
        // CHECK IF USER IS ADMIN
        // IF URL MANUALLY ENTERED - NAVIGATE TO HOME PAGE
        (user.isAdmin) ?
        <>
            <h1 className="mt-5">Admin Dashboard</h1>
            
            <CreateProduct /> &nbsp;

            <Button as={Link} to="/admin/viewAllOrders" className="mt-5" variant="outline-dark" style={{width: 20 * 10}}>
                VIEW USER ORDERS
            </Button> 

            <Table bordered className="mt-5">
                <thead>
                    <tr>
                        <th>NAME</th>
                        <th>DESCRIPTION</th>
                        <th>PRICE</th>
                        <th>AVAILABLE</th>
                        <th>ACTIONS</th>
                    </tr>
                </thead>
                <tbody>
                    { products }
                </tbody>
            </Table>
        </>
        :
        redirect('/')
    )
}
