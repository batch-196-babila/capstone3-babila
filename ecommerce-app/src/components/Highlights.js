import { Row, Col, Card } from "react-bootstrap";
import '../style/styles.css';

export default function Highlights() {
    return (
        <>
        <div className="highlights-container">
            <Row className="mt-3 mb-3 featured-container">
            <h1 className="title-header">FEATURED PRODUCTS</h1>
                <Col xs={12} md={4}>
                    <Card className="highlights-card border-0">
                        <Card.Img variant="top" src={require('../images/hp-omen.png')} />
                        <Card.Body>
                            <Card.Title>HP OMEN Laptop 17</Card.Title>
                            <Card.Text>
                                Experience fast and smooth gaming with OMEN laptops whenever you play.
                            </Card.Text>
                        </Card.Body>
                    </Card>
                </Col>
                <Col xs={12} md={4}>
                    <Card className="highlights-card border-0">
                        <Card.Img variant="top" src={require('../images/hp-envy.png')} />
                        <Card.Body>
                            <Card.Title>HP ENVY Laptop 17</Card.Title>
                            <Card.Text>
                                Increase your productivity at work by experiencing our Envy Laptops.
                            </Card.Text>
                        </Card.Body>
                    </Card>
                </Col>
                <Col xs={12} md={4}>
                    <Card className="highlights-card border-0">
                        <Card.Img variant="top" src={require('../images/hp-spectre.png')}/>
                        <Card.Body>
                            <Card.Title>HP Spectre x360</Card.Title>
                            <Card.Text>
                                Looking for lightweight and high-end laptops? Check our HP Spectre.
                            </Card.Text>
                        </Card.Body>
                    </Card>
                </Col>
            </Row>
        </div>

        </>
    );
}