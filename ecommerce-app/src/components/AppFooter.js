import '../style/styles.css';

export default function AppFooter() {
    return (
        <footer className="footer">&copy; 2022 LAPPY - All Rights Reserved</footer>
    );
}