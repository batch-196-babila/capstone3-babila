import { useEffect, useState } from 'react';
import { Container, Row, Col, ListGroup, Alert, Image} from 'react-bootstrap';
import '../style/styles.css';

export default function Profile() {

    const [firstName , setFirstName] = useState('');
    const [lastName , setLastName] = useState('');
    const [email, setEmail] = useState('');
    const [mobileNo , setMobileNo] = useState('');

    const [totalAmount, setTotalAmount] = useState('');
    const [purchasedOn, setPurchasedOn] = useState('');

    const [productId, setProductId] = useState('');
    const [quantity, setQuantity] = useState('');

    // RETRIEVE DATA
    useEffect(() => {
        fetch("http://localhost:4000/users/getUserDetails", {
            headers: {
                Authorization: `Bearer ${localStorage.getItem('token')}`
            }
        })
        .then (response => response.json())
        .then(data => {
            // console.log(data._id);
            setFirstName(data.firstName);
            setLastName(data.lastName);
            setEmail(data.email);
            setMobileNo(data.mobileNo);
        })
    }, []);

    // VIEW SINGLE / LAST ORDER ONLY
    useEffect(() => {
        fetch("http://localhost:4000/users/viewOrders", {
            headers: {
                Authorization: `Bearer ${localStorage.getItem('token')}`
            }
        })
        .then (response => response.json())
        .then(data => {
            
            // GET THE PREVIOUS ORDER (SINGLE SHOW)
            data.orders.map((order) => {
                // console.log(order);
                setTotalAmount(order.totalAmount);
                setPurchasedOn(order.purchasedOn);

                order.products.map((product) => {
                    setProductId(product._id);
                    setQuantity(product.quantity);
                    return product;
                })
                return order;
            })
            
        })
    }, []);

    return (
        <>
        <Container>
            <Row>
                <Col lg={{span:6, offset:3}}>
                    
                    <ListGroup className="profile-list-group" variant="flush">
                        <Image className="profile-logo" src={require('../images/profile.png')} />
                        <h1 className="title-header">PROFILE DETAILS</h1>
                            <ListGroup.Item></ListGroup.Item>
                            <ListGroup.Item>{firstName} {lastName}</ListGroup.Item>
                            <ListGroup.Item>{email}</ListGroup.Item>
                            <ListGroup.Item>{mobileNo}</ListGroup.Item>
                            <ListGroup.Item></ListGroup.Item>
                    </ListGroup>
                </Col>
            </Row>
        </Container>

        {(productId === '' || productId === null) ?
        
            <Container className="order-alert-container">
                <h1 className="title-header">PREVIOUS ORDER</h1>
                <Alert className="no-order-alert" variant="danger">
                    No existing order to show. {' '} Click {' '}
                    <Alert.Link href="/shop">here</Alert.Link> to place an order.
                </Alert>
            </Container>
            
        :
            <Container>
                <Row>
                    <Col lg={{span:6, offset:3}}>
                        <ListGroup as="ul" className="profile-list-group" >
                            <Image className="profile-logo" src={require('../images/order-history.png')} />
                            <h1 className="title-header">PREVIOUS ORDER</h1>
                            <ListGroup.Item as="li" active>
                                Purchased on: {purchasedOn}
                            </ListGroup.Item>
                            <ListGroup.Item as="li">
                            Product Id: { productId } - Quantity: { quantity }
                            </ListGroup.Item>
                            <ListGroup.Item as="li">Total Amount: PHP { totalAmount }</ListGroup.Item>
                        </ListGroup>
                    </Col>
                </Row>
            </Container>
        }
        </>
    );
}