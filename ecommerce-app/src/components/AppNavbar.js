import { useContext } from 'react';
import { Link } from "react-router-dom";
import { Navbar, Container, Nav } from 'react-bootstrap';
import UserContext from "../UserContext";
import '../style/styles.css';

export default function AppNavbar() {
    const {user} = useContext(UserContext);

    return (
        <Navbar className="navbar" expand="lg">
            <Container className="navbar-container">
                <Navbar.Brand as={Link} to="/"><strong>L A P P Y</strong></Navbar.Brand>
                <Navbar.Toggle aria-controls="basic-navbar-nav"  />
                <Navbar.Collapse id="basic-navbar-nav" className="justify-content-end">
                    <Nav>
                        <Nav.Link href="/">HOME</Nav.Link>
                        { (user.isAdmin === true) ?
                            <Nav.Link as={Link} to="/admin/dashboard">DASHBOARD</Nav.Link> 
                            :
                            <Nav.Link as={Link} to="/shop">SHOP</Nav.Link>
                        }    
                        { (user.id !== null) ?
                            <>
                            <Nav.Link as={Link} to="/profile">PROFILE</Nav.Link>
                            <Nav.Link as={Link} to="/logout">LOGOUT</Nav.Link>
                            </> 
                            :
                            <Nav.Link as={Link} to="/login">LOGIN</Nav.Link>
                        }
                    </Nav>
                </Navbar.Collapse>
            </Container>
        </Navbar>
    )
};