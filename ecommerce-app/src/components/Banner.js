import { useContext } from 'react';
import { Link } from "react-router-dom";
import {Button, Image} from 'react-bootstrap';
import UserContext from '../UserContext';
import '../style/styles.css';

export default function Banner() {
    // CHECK IF USER ADMIN
    // CHANGE BUTTON TO MANAGE USER IF ADMIN
    // ELSE PROCEED TO SHOP AS NORMAL USER
    const { user } = useContext(UserContext);
    
    return (
        <div className="banner-container">
            <Image className="img-banner" src={require('../images/acer-banner.jpg')} fluid="true"></Image>
            <div className="banner-description">
                <h1>Efficient and Dependable</h1>
                <p>A laptop that fits your lifestyle</p>
                { (user.isAdmin === false || user.id === null) ? 
                    <Button variant="outline-light" className="banner-btn" as={Link} to="/shop">SHOP HERE</Button>
                :
                    <Button variant="outline-light" className="banner-btn" as={Link} to="/admin/dashboard">MANAGE USER</Button>
                }
            </div>
        </div>
    );
}