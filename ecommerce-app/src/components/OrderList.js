export default function OrderList({ orderProp }) {

    const { firstName, lastName } = orderProp;
 
    return (
        orderProp.orders.map((order) =>
            order.products.map((product) =>
                <tr key={product._id}> 
                    <td>{ firstName } { lastName }</td>
                    <td>{ product._id }</td>
                    <td>{ order.totalAmount }</td>
                    <td>{ product.quantity }</td>
                </tr>
            )
        )
    );
}